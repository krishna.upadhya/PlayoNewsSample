package com.inteliment.assignment.sample.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.inteliment.assignment.sample.ILCApplication;
import com.inteliment.assignment.sample.interfaces.RxBus;
import com.inteliment.assignment.sample.network.ILCNetworkManager;

import javax.inject.Inject;

public class ILFBaseFragment extends Fragment {

    @Inject
    public ILCNetworkManager mRSNetworkManager;

    @Inject
    public RxBus mRxBus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ILCApplication) getActivity().getApplicationContext()).getRSAppComponent().inject(this);
    }

    protected int FRAG_ADD = 1;
    protected int FRAG_REPLACE = 2;
    protected int FRAG_ADD_ANIMATE = 3;
    protected int FRAG_DIALOG = 4;
    protected int FRAG_REPLACE_WITH_STACK = 5;
    protected int FRAG_ADD_WITH_STACK = 6;
}
