package com.inteliment.assignment.sample.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class ILCLocationDataModel extends BaseModel {

    public ArrayList<ILCHitsList> getHits() {
        return hits;
    }

    public void setHits(ArrayList<ILCHitsList> hits) {
        this.hits = hits;
    }

    @Expose
    private ArrayList<ILCHitsList> hits;


}
