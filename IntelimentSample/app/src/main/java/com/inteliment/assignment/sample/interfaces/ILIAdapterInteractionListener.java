package com.inteliment.assignment.sample.interfaces;

public interface ILIAdapterInteractionListener {
    public void onListItemClick(String layoutType, String data);
}
