package com.inteliment.assignment.sample.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ILFNewsDetailWebViewFragment extends ILFBaseFragment {
    private Unbinder mUnbinder;

    @BindView(R.id.title_toolbar)
    RoboTextView titleToolbar;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.webView)
    WebView mWebView;
    private String mUrl;


    @OnClick(R.id.btn_back)
    public void backPressed() {
        getActivity().onBackPressed();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_custom_layout, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        getBundleData();
        initData();
        return view;
    }

    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle.containsKey("url")) {
            mUrl = bundle.getString("url");
        }
    }

    private void initData() {
        if (!TextUtils.isEmpty(mUrl)) {
            mWebView.setWebViewClient(new WebViewClient());
            mWebView.loadUrl(mUrl);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar();

    }


    private void setupToolbar() {
        titleToolbar.setText(getActivity().getResources().getString(R.string.custom_list));
    }

}
