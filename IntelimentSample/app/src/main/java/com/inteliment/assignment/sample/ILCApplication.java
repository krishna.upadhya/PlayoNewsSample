
package com.inteliment.assignment.sample;


import android.app.Application;
import android.content.Context;

import com.inteliment.assignment.sample.dagger.DaggerILCAppComponent;
import com.inteliment.assignment.sample.dagger.ILCAppComponent;
import com.inteliment.assignment.sample.dagger.ILCAppModule;
import com.inteliment.assignment.sample.network.ILCNetworkModule;


public class ILCApplication extends Application {

    private static Context context;
    private ILCAppComponent mILCAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mILCAppComponent = DaggerILCAppComponent.builder()
                .iLCAppModule(new ILCAppModule(this))
                .iLCNetworkModule(new ILCNetworkModule(getContext()))
                .build();
    }

    public ILCAppComponent getRSAppComponent() {
        return mILCAppComponent;
    }

    public ILCApplication() {
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
