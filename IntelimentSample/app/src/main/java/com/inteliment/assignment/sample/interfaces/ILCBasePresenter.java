package com.inteliment.assignment.sample.interfaces;

public interface ILCBasePresenter<T> {
    void start();

    void resume();

    void pause();

    void stop();

    void destroy();
    void attach(T view);

    /**
     * Called when the view is destroyed to get rid of its presenter
     */
    void detach();
}
