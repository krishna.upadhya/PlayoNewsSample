package com.inteliment.assignment.sample.model;

import com.google.gson.annotations.Expose;

public class ILCLocationModel extends BaseModel {

    @Expose
    private double latitude;

    @Expose
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
