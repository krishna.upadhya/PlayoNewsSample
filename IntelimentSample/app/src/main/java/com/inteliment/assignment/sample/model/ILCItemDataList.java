package com.inteliment.assignment.sample.model;

public class ILCItemDataList {
    private String mName;
    private String color_code;

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getColor_code() {
        return color_code;
    }

    public void setColor_code(String color_code) {
        this.color_code = color_code;
    }
}
