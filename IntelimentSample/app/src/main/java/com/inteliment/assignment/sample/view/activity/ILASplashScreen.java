package com.inteliment.assignment.sample.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.inteliment.assignment.sample.R;

public class ILASplashScreen extends ILABaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        startHome();
    }

    /**
     * Start home after 400 millisecond delay
     */
    private void startHome() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(ILASplashScreen.this, ILAMainActivity.class));
                finish();
            }
        }, 4000);
    }
}
