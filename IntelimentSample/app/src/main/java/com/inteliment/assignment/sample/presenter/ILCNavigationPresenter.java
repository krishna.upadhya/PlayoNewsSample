
package com.inteliment.assignment.sample.presenter;

import android.util.Log;

import com.inteliment.assignment.sample.contract.ILCNavigationContract;
import com.inteliment.assignment.sample.model.ILCLocationDataModel;
import com.inteliment.assignment.sample.network.ILCNetworkErrorHandler;
import com.inteliment.assignment.sample.network.ILCNetworkManager;
import com.inteliment.assignment.sample.network.ILCResponseListener;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class ILCNavigationPresenter implements ILCNavigationContract.presenter {

    private ILCNetworkManager mNetworkWrapper;
    public ILCNavigationContract.View mView;
    private CompositeSubscription subscriptions;

    public ILCNavigationPresenter(ILCNetworkManager networkWrapper, ILCNavigationContract.View view) {
        mNetworkWrapper = networkWrapper;
        mView = view;
        subscriptions = new CompositeSubscription();
    }

    @Override
    public void attach(ILCNavigationContract.View view) {
        this.mView = view;
    }

    @Override
    public void detach() {
        this.mView = null;
    }


    @Override
    public void start() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {
        subscriptions.unsubscribe();
    }

    @Override
    public void destroy() {

    }

    @Override
    public void getNavigationData(String searchText, int pageNumber) {
        mView.showProgress();
        Subscription subscription = mNetworkWrapper.getNavigationList(new ILCResponseListener<ILCLocationDataModel>() {
            @Override
            public void onSuccess(ILCLocationDataModel response) {
                mView.hideProgress();
                mView.onSuccess(response);
                Log.d("KR", "response success");
            }

            @Override
            public void onFailure(Throwable error) {
                Log.d("KR", "response error");
            }

            @Override
            public void onError(Throwable e) {
                mView.hideProgress();
                mView.showError(ILCNetworkErrorHandler.getNetworkErrorMessage(e));
            }

        }, searchText, pageNumber);
        subscriptions.add(subscription);
    }
}
