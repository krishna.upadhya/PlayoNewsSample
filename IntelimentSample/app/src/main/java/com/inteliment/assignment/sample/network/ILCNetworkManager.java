

package com.inteliment.assignment.sample.network;

import com.inteliment.assignment.sample.model.ILCLocationDataModel;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Helper class to handle
 */

public class ILCNetworkManager {

    private ILCRetrofitInterface mRetrofitInterface;

    public ILCNetworkManager(ILCRetrofitInterface RetrofitInterface) {
        mRetrofitInterface = RetrofitInterface;
    }

    public Subscription getNavigationList(ILCResponseListener<ILCLocationDataModel> callback, String searchText, int pageNumber) {
        return mRetrofitInterface.getLocationData(ILCNetworkUtils.getURL(ILCNetworkUtils.REQ_LOCATION), searchText, pageNumber)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ILCLocationDataModel>>() {
                    @Override
                    public Observable<? extends ILCLocationDataModel> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(callback);
    }
}
