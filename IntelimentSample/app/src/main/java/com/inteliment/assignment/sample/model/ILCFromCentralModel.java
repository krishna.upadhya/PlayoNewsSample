package com.inteliment.assignment.sample.model;

import com.google.gson.annotations.Expose;

public class ILCFromCentralModel extends BaseModel {

    @Expose
    private String car;

    @Expose
    private String train;

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getTrain() {
        return train;
    }

    public void setTrain(String train) {
        this.train = train;
    }
}
