package com.inteliment.assignment.sample.contract;

import com.inteliment.assignment.sample.interfaces.ILCBasePresenter;
import com.inteliment.assignment.sample.interfaces.ILCBaseView;
import com.inteliment.assignment.sample.model.ILCLocationDataModel;

public interface ILCNavigationContract {
    interface View extends ILCBaseView {
        void init();

        void onSuccess(ILCLocationDataModel dataList);

    }

    interface presenter extends ILCBasePresenter<ILCNavigationContract.View> {
        void getNavigationData(String searchText, int pageNumber);

    }
}
