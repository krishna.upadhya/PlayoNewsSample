package com.inteliment.assignment.sample.interfaces;

public interface ILCBaseView {
    void showProgress();

    void hideProgress();

    void showError(int stringResource);

}
