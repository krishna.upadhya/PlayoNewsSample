package com.inteliment.assignment.sample.network;

import com.inteliment.assignment.sample.model.ILCLocationDataModel;

import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

public interface ILCRetrofitInterface {
    @GET()
    Observable<ILCLocationDataModel> getLocationData(@Url String url, @Query("query") String page, @Query("page") int pageNumber);
}
