package com.inteliment.assignment.sample.model;

import com.google.gson.annotations.Expose;

/**
 * Created by sapna on 10-10-2017.
 */

public class ILCHitsList {
    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Expose
    private String created_at;

    @Expose
    private String title;

    @Expose
    private String url;

    @Expose
    private String author;


}
