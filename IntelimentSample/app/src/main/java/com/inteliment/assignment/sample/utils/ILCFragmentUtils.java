package com.inteliment.assignment.sample.utils;

import android.support.v4.app.Fragment;

import com.inteliment.assignment.sample.view.fragment.ILFNewsDetailWebViewFragment;
import com.inteliment.assignment.sample.view.fragment.ILFNewsListFragment;

public class ILCFragmentUtils {
    public static String getFragmentTag(int type) {
        switch (type) {
            case ILCFragmentConstants.FRAGMENT_CUSTOM_LIST:
                return ILFNewsDetailWebViewFragment.class.getName();

            case ILCFragmentConstants.FRAGMENT_NAVIGATION_LIST:
                return ILFNewsListFragment.class.getName();

            default:
                return Fragment.class.getName();
        }
    }
}
