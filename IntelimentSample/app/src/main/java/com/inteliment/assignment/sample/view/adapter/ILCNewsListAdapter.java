package com.inteliment.assignment.sample.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.model.ILCHitsList;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ILCNewsListAdapter extends RecyclerView.Adapter<ILCNewsListAdapter.ViewHolder> {
    private Context mContext;


    public void setHorizontalDataList(ArrayList<ILCHitsList> mHorizontalDataList) {
        this.mHorizontalDataList = mHorizontalDataList;
        notifyDataSetChanged();
    }

    ArrayList<ILCHitsList> mHorizontalDataList;
    ILCListCClickListener mCallback;

    public interface ILCListCClickListener {
        public void onListClick(String url);
    }

    public ILCNewsListAdapter(Context context, ArrayList<ILCHitsList> horizontalDataList, ILCListCClickListener callback) {
        mContext = context;
        mHorizontalDataList = horizontalDataList;
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.button_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBtnDecorator.setText(mHorizontalDataList.get(position).getTitle());

        holder.mCreatedAt.setText(mHorizontalDataList.get(position).getCreated_at());

        holder.mAuther.setText(mHorizontalDataList.get(position).getAuthor());
    }

    @Override
    public int getItemCount() {
        if (mHorizontalDataList != null && mHorizontalDataList.size() > 0) {
            return mHorizontalDataList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.btn_decorator)
        RoboTextView mBtnDecorator;

        @BindView(R.id.created_at)
        RoboTextView mCreatedAt;

        @BindView(R.id.auther)
        RoboTextView mAuther;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view) {
            if (mCallback != null) {
                mCallback.onListClick(mHorizontalDataList.get(getAdapterPosition()).getUrl());
            }
        }
    }
}
