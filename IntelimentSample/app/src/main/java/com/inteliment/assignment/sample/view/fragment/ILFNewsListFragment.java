package com.inteliment.assignment.sample.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.inteliment.assignment.sample.R;
import com.inteliment.assignment.sample.contract.ILCNavigationContract;
import com.inteliment.assignment.sample.interfaces.FragmentInteractionListener;
import com.inteliment.assignment.sample.model.ILCHitsList;
import com.inteliment.assignment.sample.model.ILCLocationDataModel;
import com.inteliment.assignment.sample.presenter.ILCNavigationPresenter;
import com.inteliment.assignment.sample.utils.ILCFileUtils;
import com.inteliment.assignment.sample.utils.ILCFragmentConstants;
import com.inteliment.assignment.sample.view.adapter.ILCNewsListAdapter;
import com.inteliment.assignment.sample.view.widgets.RoboTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ILFNewsListFragment extends ILFBaseFragment implements ILCNavigationContract.View, ILCNewsListAdapter.ILCListCClickListener {

    @BindView(R.id.title_toolbar)
    RoboTextView titleToolbar;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progress_lyt)
    RelativeLayout mProgressLyt;

    @BindView(R.id.pagination_progress)
    RelativeLayout mPaginationProgress;

    @BindView(R.id.btn_list)
    RecyclerView mButtonList;

    @BindView(R.id.search_txt)
    EditText mSearchText;

    @BindView(R.id.btn_search)
    Button mSearchButton;
    private int pageNumber = 1;
    private boolean isLoading;
    private LinearLayoutManager linerLayoutManager;
    private String searchedText;
    private ArrayList<ILCHitsList> finalListData = new ArrayList<>();

    @OnClick(R.id.btn_search)
    public void onSearchButtonClick() {
        searchedText = mSearchText.getText().toString();
        if (!TextUtils.isEmpty(searchedText)) {
            if (ILCFileUtils.isNetworkAvailable(getActivity())) {
                isLoading = true;
                mRegisterUserPresenter.getNavigationData(searchedText, pageNumber);
            } else {
                Toast.makeText(getActivity(), "Please check your network connection", Toast.LENGTH_SHORT).show();
            }
            showProgress();
        } else {
            Toast.makeText(getActivity(), "Enter Search Text", Toast.LENGTH_SHORT).show();
        }
    }

    private ArrayList<ILCHitsList> mNewsList;

    ILCNewsListAdapter ilcButtonListAdapter;

    private Unbinder mUnbinder;

    private ILCNavigationPresenter mRegisterUserPresenter;

    private FragmentInteractionListener fragmentInteractionListener;


    @OnClick(R.id.btn_back)
    public void backPressed() {
        getActivity().onBackPressed();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_list, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar();
        init();
    }

    private void setupToolbar() {
        titleToolbar.setText(getActivity().getResources().getString(R.string.navigation_list));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentInteractionListener = (FragmentInteractionListener) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        linerLayoutManager = new LinearLayoutManager(getContext());
        mButtonList.setHasFixedSize(true);
        mButtonList.setLayoutManager(linerLayoutManager);
        mRegisterUserPresenter = new ILCNavigationPresenter(mRSNetworkManager, this);
        mButtonList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastVisibleItemPosition = linerLayoutManager.findLastVisibleItemPosition();

                if (lastVisibleItemPosition == ilcButtonListAdapter.getItemCount() - 1) {
                    if ((!isLoading)) {
                        isLoading = true;
                        pageNumber++;
                        mRegisterUserPresenter.getNavigationData(searchedText, pageNumber);
                        mPaginationProgress.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        ilcButtonListAdapter = new ILCNewsListAdapter(getActivity(), finalListData, this);
        mButtonList.setAdapter(ilcButtonListAdapter);


    }

    @Override
    public void onSuccess(ILCLocationDataModel dataList) {
        hideProgress();
        isLoading = false;
        mPaginationProgress.setVisibility(View.GONE);
        initList(dataList);
    }

    private void initList(ILCLocationDataModel dataList) {
        if (finalListData.size() == 0) {
            finalListData = dataList.getHits();
        } else {
            finalListData.addAll(dataList.getHits());
        }
        ilcButtonListAdapter.setHorizontalDataList(finalListData);
    }

    @Override
    public void showProgress() {
        mProgressLyt.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressLyt.setVisibility(View.GONE);
    }

    @Override
    public void showError(int stringResource) {

    }

    @Override
    public void onListClick(String url) {

        if (fragmentInteractionListener != null) {
            Bundle bundle = new Bundle();
            bundle.putString("url", url);
            fragmentInteractionListener.setFragment(bundle, ILCFragmentConstants.FRAGMENT_CUSTOM_LIST, FRAG_ADD_WITH_STACK);
        }
    }
}
