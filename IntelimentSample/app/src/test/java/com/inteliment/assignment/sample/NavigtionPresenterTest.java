package com.inteliment.assignment.sample;



import com.inteliment.assignment.sample.contract.ILCNavigationContract;
import com.inteliment.assignment.sample.presenter.ILCNavigationPresenter;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.mockito.Mockito.mock;

public class NavigtionPresenterTest {
    @Test
    public void testAttach() {
        ILCNavigationPresenter mainActivityPresenter = new ILCNavigationPresenter(null, null);
        assertNull(mainActivityPresenter.mView);

        mainActivityPresenter.attach(mock(ILCNavigationContract.View.class));
        assertNotNull(mainActivityPresenter.mView);
    }

    @Test
    public void testDetach() {
        ILCNavigationPresenter mainActivityPresenter = new ILCNavigationPresenter(null, null);
        mainActivityPresenter.attach(mock(ILCNavigationContract.View.class));
        assertNotNull(mainActivityPresenter.mView);

        mainActivityPresenter.detach();
        assertNull(mainActivityPresenter.mView);
    }
}
